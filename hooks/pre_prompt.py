import sys

from color import color
from command import cmd

DIRENV_SNIPPET = """
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}
layout_python-venv() {
    local python=${1:-python3}
    [[ $# -gt 0 ]] && shift
    unset PYTHONHOME
    if [[ -n $VIRTUAL_ENV ]]; then
        VIRTUAL_ENV=$(realpath "${VIRTUAL_ENV}")
    else
        local python_version
        python_version=$("$python" -c "import platform; print(platform.python_version())")
        if [[ -z $python_version ]]; then
            log_error "Could not detect Python version"
            return 1
        fi
        VIRTUAL_ENV=$PWD/.env
    fi
    export VIRTUAL_ENV
    if [[ ! -d $VIRTUAL_ENV ]]; then
        log_error "no venv found; Searching for $VIRTUAL_ENV"
        unset VIRTUAL_ENV
    else
        PATH="${VIRTUAL_ENV}/bin:${PATH}"
        export PATH
    fi
}
"""

POETRY_WARNING = f"""
{color.BOLD}{color.RED}Poetry was not found on the system.{color.END}

Poetry is a dependency for this project template.
Please visit the poetry documentation(https://python-poetry.org/) to follow the
installation guide or install directly withLorem ipsum:

> {color.BOLD}pipx install poetry{color.END}
"""

MAKEFILE_WARNING = f"""
{color.BOLD}{color.RED}Make was not found on the system.{color.END}

The make utility is a dependency for this project template.

"""

DIRENV_WARNING = f"""
{color.BOLD}{color.YELLOW}Direnv was not found on the system.{color.END}

The direnv utility is optional but recommended. It enables the functionality of
automatically activating the virtual environment when accessing the directory.

For this to work, install the direnv,
see https://direnv.net/docs/installation.html#from-system-packages. When
completed add the following snippet to you bashrc, zshrc ect.:

{DIRENV_SNIPPET}

When first accessing the directory direnv will ask to allow the execution of the
.envrc config. See the description in the console or add by `direnv allow`.
"""

GIT_WARNING = f"""
{color.BOLD}{color.YELLOW}Git was not found on the system.{color.YELLOW}

Git is a dependency for githooks, if this is needed please install git:

See https://github.com/git-guides/install-git

"""


SEPARATOR = "  • "


def fatal(message: str) -> None:
    print(message)
    sys.exit(-1)


def warning(message: str) -> None:
    print(message)


def main() -> None:
    if not cmd(["poetry", "--version"]):
        fatal(POETRY_WARNING)
    else:
        print(f"{SEPARATOR}Found poetry")

    if not cmd(["make", "--version"]):
        fatal(MAKEFILE_WARNING)
    else:
        print(f"{SEPARATOR}Found make")

    if not cmd(["direnv", "--version"]):
        warning(DIRENV_WARNING)
    else:
        print(f"{SEPARATOR}Found direnv")

    if not cmd(["git", "--version"]):
        fatal(GIT_WARNING)
    else:
        print(f"{SEPARATOR}Found git")

    sys.exit(0)


if __name__ == "__main__":
    main()
