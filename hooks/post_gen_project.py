import os
import shutil
import subprocess
from enum import Enum
from pathlib import Path
from typing import List, Optional

DEVELOPMENT = ["isort", "black", "flake8", "mypy"]
DEV_GROUP = "dev"
DOCS = ["sphinx"]
TESTS = ["pytest", "pytest-cov"]
SECURITY = ["bandit"]
PRE_COMMIT = ["pre-commit"]


def cmd(commands: List[str]) -> None:
    subprocess.run(commands, capture_output=False, check=True)


def venv_cmd(commands: List[str]) -> None:
    subprocess.run(["poetry", "run"] + commands, capture_output=False, check=True)


def install(package: str, group: Optional[str] = None) -> None:
    if group:
        cmd(["poetry", "add", package, f"--group={group}"])
    else:
        cmd(["poetry", "add", package])


def install_packages(packages: List[str], group: Optional[str] = None) -> None:
    for package in packages:
        install(package, group=group)


def remove_paths(paths: List[str]) -> None:
    for path in paths:
        path = path.strip()
        if path and os.path.exists(path):
            os.unlink(path) if os.path.isfile(path) else shutil.rmtree(path)


def rename_path(src_path: Path, dst_path: Path) -> None:
    os.rename(src_path, dst_path)


def docker():
    docker_base = str("{{ cookiecutter.docker_base }}")
    if docker_base == "alpine":
        rename_path(Path("Dockerfile.alpine"), Path("Dockerfile"))
    elif docker_base == "default":
        rename_path(Path("Dockerfile.default"), Path("Dockerfile"))
    elif docker_base == "slim":
        rename_path(Path("Dockerfile.slim"), Path("Dockerfile"))
    else:
        raise RuntimeError(
            f"If statements not exhaustive, docker_base was: {docker_base}"
        )
    remove_paths(["Dockerfile.alpine", "Dockerfile.default", "Dockerfile.slim"])


def docs():
    """Create new docs project"""
    venv_cmd(
        [
            "sphinx-quickstart",
            "--sep",
            "--release=v0.0.0",
            "--project={{ cookiecutter.project_name }}",
            "--author={{ cookiecutter.project_author_name }}",
            "--language=en",
            "docs",
        ]
    )
    rename_path(
        Path("doc_conf") / Path("conf.py"), Path("docs/source") / Path("conf.py")
    )
    remove_paths(["doc_conf/"])


def pre_commit_hooks():
    install_commit_hooks = bool("{{ cookiecutter.use_commit_hooks }}")
    if install_commit_hooks:
        install_packages(PRE_COMMIT, group=DEV_GROUP)
        cmd(["git", "init"])
        venv_cmd(["pre-commit", "install"])
    else:
        remove_paths([".pre-commit-config.yaml"])


def ci_install():
    ci_system = str("{{ cookiecutter.ci_system }}")
    if ci_system == "gitlab":
        remove_paths([".github"])
    elif ci_system == "github":
        remove_paths([".gitlab/", ".gitlab-ci.yml"])
    else:
        raise RuntimeError(
            f"If statements not exhaustive, expected gitlab/github, got {ci_system}"
        )


def remove_main():
    remove_paths(
        [
            "src/{{cookiecutter.project_slug}}/main.py",
            "{{cookiecutter.project_slug}}/main.py",
        ]
    )


def django():
    """Install django packages and remove unused directory"""
    print("Creating project from django template!")

    install_packages(["django", "djangorestframework", "django-health-check"])
    install_packages(["pytest-django", "django_stubs"], group=DEV_GROUP)

    remove_paths(["flask", "flat", "src"])

    venv_cmd(
        [
            "django-admin",
            "startproject",
            "--template",
            "django/project_template/",
            "{{ cookiecutter.project_slug }}",
            ".",
        ]
    )
    remove_paths(["django"])


def flask():
    """Remove everything not related to flask and install python packages related to flask"""
    print("Creating project from flask template!")

    remove_paths(["django", "flat", "src"])
    install_packages(["flask"])


def flat():
    project_slug = str("{{ cookiecutter.project_slug }}")
    install_packages(["twine"], group=DEV_GROUP)

    print("Creating project from flat template")
    remove_paths(["django", "flask", "src", "templates"])
    rename_path(Path("flat"), Path(project_slug))


def src():
    print("Creating project from src template")
    install_packages(["twine"], group=DEV_GROUP)
    remove_paths(["django", "flask", "flat"])


class ProjectTypes(Enum):
    django = "django"
    flask = "flask"
    app_flat = "app_flat"
    app_src = "app_src"
    lib_flat = "lib_flat"
    lib_src = "lib_src"


def main():
    project_type = str("{{ cookiecutter.project_type }}")

    match project_type:
        case ProjectTypes.app_flat.value:
            flat()
        case ProjectTypes.app_src.value:
            src()
        case ProjectTypes.django.value:
            django()
        case ProjectTypes.flask.value:
            flask()
        case ProjectTypes.lib_flat.value:
            remove_main()
            flat()
        case ProjectTypes.lib_src.value:
            remove_main()
            src()
        case _:
            raise RuntimeError(
                "Case match was not exhaustive, "
                f"value of project_type: {project_type}"
            )

    install_packages(DEVELOPMENT + TESTS + DOCS + SECURITY, group=DEV_GROUP)
    docs()
    pre_commit_hooks()
    ci_install()
    docker()


if __name__ == "__main__":
    main()
