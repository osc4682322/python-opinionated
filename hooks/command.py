import subprocess
from typing import List


def cmd(commands: List[str]) -> bool:
    try:
        subprocess.run(commands, capture_output=True, check=True)
        return True
    except Exception:
        return False
