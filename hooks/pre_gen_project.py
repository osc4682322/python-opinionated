from enum import Enum


class ProjectTypes(Enum):
    app_flat = "app_flat"
    app_src = "app_src"
    django = "django"
    flask = "flask"
    lib_flat = "lib_flat"
    lib_src = "lib_src"


def app_flat():
    pass


def app_src():
    pass


def django():
    pass


def flask():
    pass


def lib_flat():
    pass


def lib_src():
    pass


def main():
    project_type = str("{{ cookiecutter.project_type }}")

    match project_type:
        case ProjectTypes.django.value:
            django()
        case ProjectTypes.flask.value:
            flask()
        case ProjectTypes.app_flat.value:
            app_flat()
        case ProjectTypes.app_src.value:
            app_src()
        case ProjectTypes.lib_flat.value:
            lib_flat()
        case ProjectTypes.lib_src.value:
            lib_src()
        case _:
            raise RuntimeError(f"Case match was not exhaustive, {project_type}")


if __name__ == "__main__":
    main()
